I'm really new to Ruby/Rails, so my standard is low yet.


# Features/Fixes:

1. Please, create a feature branch.
2. Once stable cover it with Unit and Functional tests.
3. Submit to pull request and we will do a Code Review.
4. Once accepted, we will merge the code.


# Style:

I'm not tied to any style.
I'm using Rubymine linters to help me though.
Except when I disagree with it's tips.

Try to follow the patterns already stablished.
I'm accepting suggestions of styles.
But once any is choosed, all the code must match.

I do like auto formatters, even if it don't please everybody, at least
it do an average job wich spare us from some style pains and let us focuses
in production.