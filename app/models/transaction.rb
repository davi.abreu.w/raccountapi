class Transaction < ApplicationRecord
  MINIMUM_AMOUNT = 0
  ERROR_AMOUNT = "- Must be greater than #{MINIMUM_AMOUNT}.".freeze

  belongs_to :source_account, class_name: Account.name, foreign_key: :source_account_id
  belongs_to :destination_account, class_name: Account.name, foreign_key: :destination_account_id

  validates_associated :source_account
  validates_associated :destination_account

  validates_numericality_of :amount, greater_than: MINIMUM_AMOUNT, message: ERROR_AMOUNT
end
