class Account < ApplicationRecord

  def credits
    Transaction.where(destination_account: self)
  end

  def credit_balance
    credits.sum(:amount)
  end

  def debits
    Transaction.where(source_account: self)
  end

  def debit_balance
    debits.sum(:amount)
  end

  def balance
    credit_balance - debit_balance
  end

end
