require './lib/account_api'

module Api
  module V1

    class TransactionsController < ApplicationController

      def create
        keys = %i[account_id to_account_id amount]
        @result = AccountApi.validate keys, params, check_if_int: true
        return render_api @result.http unless @result.ok?

        @result = AccountApi.account_transfer(
          params[:account_id].to_i,
          params[:to_account_id].to_i,
          params[:amount].to_i
        )
        render_api @result.http
      end
    end
  end
end
