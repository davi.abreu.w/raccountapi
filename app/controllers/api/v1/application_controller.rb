require './lib/account_api'

module Api
  module V1

    class ApplicationController < ActionController::API
      before_action :authenticate_request
      attr_reader :current_user

      private

      def render_api(status)
        render 'api/_default.json.jbuilder', status: status
      end

      def authenticate_request
        @current_user = AuthorizeApiRequest.call(request.headers).result
        unless @current_user
          @result = Api::AccountApi.unauthorized
          render_api @result.http
        end
      end

    end

  end
end
