require './lib/account_api'

module Api
  module V1

    class AuthenticationsController < ApplicationController
      skip_before_action :authenticate_request

      def authenticate
        keys = %i[login password]
        @result = Api::AccountApi.validate keys, params
        return render_api @result.http unless @result.ok?

        command = AuthenticateUser.call(params[:login], params[:password])

        @result = if command.success?
                    Api::ApiResult.new result: command.result
                  else
                    Api::AccountApi.unauthorized
                  end
        render_api @result.http
      end

    end

  end
end
