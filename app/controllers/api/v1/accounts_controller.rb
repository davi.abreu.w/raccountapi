require './lib/account_api'

module Api
  module V1

    class AccountsController < ApplicationController
      before_action :set_default_format

      def show
        keys = %i[id]
        @result = AccountApi.validate keys, params, check_if_int: true
        return render_api @result.http unless @result.ok?

        @result = AccountApi.account_balance(params[:id].to_i)
        render_api @result.http
      end

      def set_default_format
        request.format = :json
      end

    end

  end
end


