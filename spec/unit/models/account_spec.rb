# frozen_string_literal: true

describe 'Creating a', Account do

  last_id = nil

  context 'do not have arguments' do
    it 'and should be okay.' do
      acc = Account.create
      expect( acc ).to be
      expect( acc.id ).to be
      expect( acc.valid? ).to be
      last_id = acc.id
    end
  end

  context 'and next one' do
    it 'should increment id.' do
      acc = Account.create
      expect( acc ).to be
      expect( acc.id ).to be
      expect( acc.valid? ).to be
      expect( last_id == acc.id - 1 ).to be
    end
  end

end

describe 'Calculating transactions of', Account do
  before(:all) do
    @acc = Account.create
    @aux = Account.create
    8.times do |_|
      Transaction.create(source_account: @aux,
                         destination_account: @acc,
                         amount: 100)
    end
    2.times do |_|
      Transaction.create(source_account: @acc,
                         destination_account: @aux,
                         amount: 100)
    end
  end

  context 'with 8 credits of 100 and 2 debits of 100' do
    it 'should have 800 of credit balance.' do
      expect( @acc.credit_balance ).to eq 800
    end
    it 'should have 200 of debit balance.' do
      expect( @acc.debit_balance ).to eq 200
    end
    it 'should have 600 of balance.' do
      expect( @acc.balance ).to eq 600
    end
  end
end