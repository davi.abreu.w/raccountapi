# frozen_string_literal: true

describe 'Creating a', Transaction do
  fixtures :accounts, :transactions

  context 'with valid accounts and a positive amount' do
    it 'should be okay.' do
      expect( transactions(:basic) ).to be
    end
  end

  context 'with a negative or 0 amount' do
    it 'should fail validation.' do
      %i[negative_amount zero_amount].each do |transaction|
        t = transactions(transaction)
        expect( t.invalid? ).to be
        expect( t.errors.size ).to eq 1
        expect( t.errors.key?('amount') ).to be
      end
    end
  end

  context 'with an invalid source_account_id' do
    it 'should fail validation.' do
      t = transactions(:invalid_source)
      expect( t.invalid? ).to be
      expect( t.errors.size ).to eq 1
      expect( t.errors.key?('source_account') ).to be
    end
  end

  context 'with an invalid destination_account_id' do
    it 'should fail validation.' do
      t = transactions(:invalid_destination)
      expect( t.invalid? ).to be
      expect( t.errors.size ).to eq 1
      expect( t.errors.key?('destination_account') ).to be
    end
  end

end
