# frozen_string_literal: true

require './lib/account_api'

describe 'Checking parameters with', Api::AccountApi do

  api = Api::AccountApi

  context 'using integers and checking for int' do
    it 'should return success.' do
      expected_keys = [:a, :b]
      valid_params = {a: "1", b: "2"}
      api_result = api.validate expected_keys, valid_params, check_if_int: true
      expect( api_result.status ).to eq Api::ApiResult::SUCCESS_STATUS.except(:http)
    end
  end

  context 'using anything else than integers and checking for int' do
    it 'should return ParametersError.status.' do
      expected_keys = [:a, :b]
      invalid_params = {a: "aa", b: 1.2}
      expected_exception = Api::Error::ParametersError.new invalid_params.keys.join(", ")
      api_result = api.validate expected_keys, invalid_params, check_if_int: true
      expect( api_result.status ).to eq expected_exception.status.except(:http)
    end
  end

  context 'using anything else than integers and accepting anything' do
    it 'should return success.' do
      expected_keys = [:a, :b]
      invalid_params = {a: "aa", b: 1.2}
      api_result = api.validate expected_keys, invalid_params
      expect( api_result.status ).to eq Api::ApiResult::SUCCESS_STATUS.except(:http)
    end
  end

  context 'checking for int and missing keys' do
    it 'should return ParametersMissing.status (priority).' do
      expected_keys = [:a, :b]
      invalid_params = {a: "abc"}
      expected_exception = Api::Error::ParametersMissing.new ["b"].join(", ")
      api_result = api.validate expected_keys, invalid_params, check_if_int: true
      expect( api_result.status ).to eq expected_exception.status.except(:http)
    end
  end

end

describe 'Consulting account balance with', Api::AccountApi do
  fixtures :accounts, :transactions

  api = Api::AccountApi

  context 'using an existent account id' do
    it 'should return the balance with success.' do
      transactions_amount = transactions(:basic).amount
      api_result = api.account_balance accounts(:two).id
      expect( api_result.result ).to eq transactions_amount
      expect( api_result.status ).to eq Api::ApiResult::SUCCESS_STATUS.except(:http)
    end
  end

  context 'using an inexistent account id' do
    it 'should return Api::Error::InexistentAccountID.status.' do
      unexistent_id = 1000
      excpected_exception = Api::Error::InexistentAccountID.new(1000)

      api_result = api.account_balance unexistent_id
      expect( api_result.result ).to be_nil
      expect( api_result.status ).to eq excpected_exception.status.except(:http)
    end
  end

end

describe 'Making a transfer with', Api::AccountApi do
  fixtures :accounts, :transactions

  api = Api::AccountApi

  context 'using root account' do
    it 'should be successful.' do
      root = accounts(:root)
      receiver = accounts(:receiver)
      amount = 1000

      expect( root.balance ).to eq 0

      expect( receiver.balance ).to eq 0
      expect( receiver.debits.size ).to eq 0
      expect( receiver.credits.size ).to eq 0

      api.account_transfer(root.id, receiver.id, amount)

      expect( receiver.balance ).to eq amount
      expect( receiver.debits.size ).to eq 0
      expect( receiver.credits.size ).to eq 1
    end
  end

  context 'to a root account' do
    it 'should be successful but must not effectively do the opperation.' do
      root = accounts(:root)
      sender = accounts(:two)
      amount = transactions(:basic).amount

      expect( root.balance ).to eq 0
      expect( sender.balance ).to eq amount
      expect( root.credits.size ).to eq 0
      expect( sender.debits.size ).to eq 0

      api_result = api.account_transfer(sender.id, root.id, amount)
      expect( api_result.status ).to eq Api::ApiResult::SUCCESS_STATUS.except(:http)

      expect( root.balance ).to eq 0
      expect( sender.balance ).to eq amount
      expect( root.credits.size ).to eq 0
      expect( sender.debits.size ).to eq 0
    end
  end

  context 'using normal accounts with sufficient balance' do
    it 'should be successful.' do
      sender = accounts(:two)
      receiver = accounts(:receiver)
      amount = transactions(:basic).amount

      expect( receiver.balance ).to eq 0
      expect( receiver.debits.size ).to eq 0
      expect( receiver.credits.size ).to eq 0

      api.account_transfer(sender.id, receiver.id, amount)

      expect( receiver.balance ).to eq amount
      expect( receiver.debits.size ).to eq 0
      expect( receiver.credits.size ).to eq 1
    end
  end

  context 'from an account to the same account' do
    it 'should return Api::Error::SameAccountTransfer.' do
      sender = accounts(:two)
      receiver = accounts(:two)
      amount = transactions(:basic).amount

      expected_error = Api::Error::SameAccountTransfer.new

      initial_sender_debit = sender.debits.size
      initial_receiver_credtis = receiver.credits.size

      api_result = api.account_transfer(sender.id, receiver.id, amount)
      expect( api_result.status ).to eq expected_error.status.except(:http)

      expect( sender.debits.size ).to eq initial_sender_debit
      expect( receiver.credits.size ).to eq initial_receiver_credtis
    end
  end

  context 'with negative or 0 amount' do
    it 'should return Api::Error::LessThanMinimumAmount.status.' do
      root_id = accounts(:root).id
      receiver_id = accounts(:receiver).id
      invalid_amounts = [-100, 0]
      expected_exception = Api::Error::LessThanMinimumAmount.new

      invalid_amounts.each do |amount|
        api_result = api.account_transfer(root_id, receiver_id, amount)
        expect( api_result.result ).to be_nil
        expect( api_result.status ).to eq expected_exception.status.except(:http)
      end
    end
  end

  context 'to/from unexistent account' do
    it 'should return Api::Error::InexistentAccountID.status.' do
      root_id = accounts(:root).id
      unexistent_id = 1000
      expected_exception = Api::Error::InexistentAccountID.new(unexistent_id)
      pairs = [
        {sender: root_id, receiver: unexistent_id},
        {sender: unexistent_id, receiver: root_id}
      ]
      amount = 1000

      pairs.each do |pair|
        api_result = api.account_transfer(pair[:sender], pair[:receiver], amount)
        expect(api_result.result).to be_nil
        expect(api_result.status).to eq expected_exception.status.except(:http)
      end
    end
  end

  context 'from an account with insufficient balance' do
    it 'should return Api::Error::InsufficientBalance.status
        except when is from root.' do
      root = accounts(:root)
      sender = accounts(:sender)
      receiver = accounts(:receiver)
      expected_exception = Api::Error::InsufficientBalance.new(sender.id)
      amount = 1000

      expect( receiver.balance ).to eq 0
      expect( receiver.debits.size ).to eq 0
      expect( receiver.credits.size ).to eq 0

      api_result = api.account_transfer(sender.id, receiver.id, amount)
      expect( api_result.result ).to be_nil
      expect( api_result.status ).to eq expected_exception.status.except(:http)
      expect( receiver.balance ).to eq 0
      expect( receiver.debits.size ).to eq 0
      expect( receiver.credits.size ).to eq 0

      api_result = api.account_transfer(root.id, receiver.id, amount)
      expect( api_result.result ).to be_nil
      expect( api_result.status ).to eq Api::ApiResult::SUCCESS_STATUS.except(:http)
      expect( receiver.balance ).to eq amount
      expect( receiver.debits.size ).to eq 0
      expect( receiver.credits.size ).to eq 1
    end
  end

end