require './lib/account_api'

def authenticate(params = {})
  post '/api/v1/authenticate', params: params
end

def validate_user(user)
  User.find_by_login(user.login).destroy
  User.create!(login: user.login , password: user.password_digest, password_confirmation: user.password_digest)
end

def valid_token
  User.create! login: 'x', password: 'x', password_confirmation: 'x'
  authenticate "login": 'x', "password": 'x'
  JSON.parse(response.body)['response']['result']
end

RSpec.describe 'Authenticate', :type => :request do
  fixtures :users

  results = Api::ApiResult
  errors = Api::Error
  context 'passing valid parameters' do
    it 'should return success status.' do
      user = users(:test)
      validate_user user
      params = { "login": user.login, "password": user.password_digest }
      expected_result = results::SUCCESS_STATUS

      authenticate params
      body = JSON.parse(response.body)
      expect( body['response']['result'] ).to be_a String
      expect( body['response']['status']['code'] ).to eq expected_result[:code]
      expect( response.status ).to eq expected_result[:http]
    end
  end

  context 'passing wrong parameters' do
    it 'should return unauthorized status.' do
      user = users(:test)
      params = { "login": user.login + 'x', "password": user.password_digest + 'x' }
      expected_result = errors::Unauthorized

      authenticate params
      body = JSON.parse(response.body)
      expect( body['response']['result'] ).to be_nil
      expect( body['response']['status']['code'] ).to eq expected_result::CODE
      expect( response.status ).to eq expected_result::HTTP
    end
  end

  context 'missing parameters' do
    it 'should return parameter missing status.' do
      user = users(:test)
      expected_result = errors::ParametersMissing

      authenticate
      body = JSON.parse(response.body)
      expect( body['response']['result'] ).to be_nil
      expect( body['response']['status']['code'] ).to eq expected_result::CODE
      expect( response.status ).to eq expected_result::HTTP
    end
  end

end

RSpec.describe 'Consult balance', :type => :request do
  fixtures :accounts, :transactions

  before(:each) do
    @token = valid_token
  end

  results = Api::ApiResult
  errors = Api::Error

  context 'from inexistent account' do
    it 'should return inexistent account status.' do
      inexistent_id = 1000
      expected_result = errors::InexistentAccountID

      get "/api/v1/accounts/#{inexistent_id}", headers: { "Authorization": @token }
      body = JSON.parse(response.body)
      expect( body['response']['result'] ).to be_nil
      expect( body['response']['status']['code'] ).to eq expected_result::CODE
      expect( response.status ).to eq expected_result::HTTP
    end
  end

  context 'from existent account' do
    it 'should return success status and amount.' do
      existent_id = accounts(:two).id
      amount = transactions(:basic).amount
      expected_result = results::SUCCESS_STATUS

      get "/api/v1/accounts/#{existent_id}", headers: { "Authorization": @token }
      body = JSON.parse(response.body)
      expect( body['response']['result'] ).to eq amount
      expect( body['response']['status']['code'] ).to eq expected_result[:code]
      expect( response.status ).to eq expected_result[:http]
    end
  end
end

RSpec.describe 'Transfer', :type => :request do
  fixtures :accounts, :transactions

  before(:each) do
    @token = valid_token
  end

  results = Api::ApiResult
  errors = Api::Error

  context 'sufficient amount' do
    it 'should return success account status.' do
      sender = accounts(:two)
      receiver = accounts(:receiver)
      amount = transactions(:basic).amount
      expected_result = results::SUCCESS_STATUS

      expect( sender.balance ).to eq amount
      expect( receiver.balance ).to eq 0
      post "/api/v1/accounts/#{sender.id}/transfer",
           headers: { "Authorization": @token },
           params: { "to_account_id": receiver.id, "amount": amount }
      body = JSON.parse(response.body)
      expect( body['response']['result'] ).to be_nil
      expect( body['response']['status']['code'] ).to eq expected_result[:code]
      expect( response.status ).to eq expected_result[:http]
      expect( sender.balance ).to eq 0
      expect( receiver.balance ).to eq amount
    end
  end

  context 'insufficient amount' do
    it 'should return error and do nothing.' do
      sender = accounts(:two)
      receiver = accounts(:receiver)
      amount = transactions(:basic).amount
      expected_result = errors::InsufficientBalance

      expect( sender.balance ).to eq amount
      expect( receiver.balance ).to eq 0
      post "/api/v1/accounts/#{sender.id}/transfer",
           headers: { "Authorization": @token },
           params: { "to_account_id": receiver.id, "amount": amount + 1 }
      body = JSON.parse(response.body)
      expect( body['response']['result'] ).to be_nil
      expect( body['response']['status']['code'] ).to eq expected_result::CODE
      expect( response.status ).to eq expected_result::HTTP
      expect( sender.balance ).to eq amount
      expect( receiver.balance ).to eq 0
    end
  end
end
