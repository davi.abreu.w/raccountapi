FROM ruby:2.6.3-stretch

ENV RACCOUNT_DIR /app/raccount
RUN mkdir -p $RACCOUNT_DIR
WORKDIR $RACCOUNT_DIR

COPY . .

RUN bundle install

CMD ["./scripts/start"]
