# frozen_string_literal: true

module Api
  # Minimum amount is the minimum value accepted to transfer
  MINIMUM_AMOUNT = 0

  # Root account ID can do any transference without balance check
  ROOT_ACCOUNT_ID = 0

  module Error
    MSG_PARAM = 'Params must be integers (%s)'
    MSG_MISSING = 'Params are required (%s)'
    MSG_UNAUTHORIZED = 'Not authorized.'
    MSG_SAME_ACCOUNT = 'The source and destiny accounts to transference must be different.'
    MSG_ACCOUNT_ID = "The account id '%s' do not exists."
    MSG_MINIMUM = 'Operation must exced minimum amount (%s).' % MINIMUM_AMOUNT
    MSG_BALANCE = "Insufficient credit to account id '%s'."

    class ApiError < StandardError
      CODE = nil
      MSG = 'Standard ApiError message'
      HTTP = 500

      def initialize(msg = nil)
        super self.class::MSG % msg
      end

      def code
        self.class::CODE
      end

      def http
        self.class::HTTP
      end

      def status
        { code: code, message: message, http: http }
      end
    end

    class SameAccountTransfer < ApiError; CODE = 50; HTTP = 400; MSG = MSG_SAME_ACCOUNT end
    class LessThanMinimumAmount < ApiError; CODE = 100; HTTP = 400; MSG = MSG_MINIMUM end
    class InsufficientBalance < ApiError; CODE = 150; HTTP = 400; MSG = MSG_BALANCE end
    class Unauthorized < ApiError; CODE = 200; HTTP = 401; MSG = MSG_UNAUTHORIZED end
    class InexistentAccountID < ApiError; CODE = 250; HTTP = 404; MSG = MSG_ACCOUNT_ID end
    class ParametersError < ApiError; CODE = 300; HTTP = 422; MSG = MSG_PARAM end
    class ParametersMissing < ApiError; CODE = 350; HTTP = 422; MSG = MSG_MISSING end
  end

  class ApiResult
    # This is the standard response of our API
    # It consist of:
    # * A result (value) if applicable
    # * A status (code, message)
    # * A http status code

    attr_reader :result, :status, :http

    SUCCESS_STATUS = {code: 0, message: 'Success', http: 200 }.freeze
    UNEXPECTED_STATUS = { code: -1, message: 'Unexpected error.', http: 500 }.freeze

    def initialize(result: nil, exception: nil)
      @result = result
      @exception = exception
      @status = compute_status
      @http = extract_http
      @serialized = { result: result, status: @status }
    end

    def ok?
      @status[:code] == SUCCESS_STATUS[:code]
    end

    private

    def extract_http
      # Must have setted status
      @status.delete(:http)
    end

    def compute_status
      return SUCCESS_STATUS.dup if @exception.nil?

      unless @exception.is_a?(Error::ApiError)
        puts @exception.backtrace
        return UNEXPECTED_STATUS.dup
      end

      @exception.status
    end

  end

  class AccountApi

    def self.validate( keys, params, check_if_int = false )
      # 'Keys' are the expected keys contained in 'params'
      # The 'params' is a hash with only strings as values
      # If, check_if_int is enabled, all the keys must exist
      #   and the values must be integers
      #
      #  This can give 2 errors:
      #  * Missing Keys
      #  * Must be integer
      #     Where Missing Keys have priority

      key_missing = []
      key_errors = []
      keys.each do |key|
        unless params.key? key
          key_missing << key
          next
        end
        key_errors << key if (params[key] =~ /^[0-9]+$/).nil? && check_if_int
      end
      error = Error::ParametersMissing.new(key_missing.join(', ')) unless key_missing.empty?
      error = Error::ParametersError.new(key_errors.join(', ')) unless error || key_errors.empty?
      ApiResult.new exception: error
    end

    def self.unauthorized
      ApiResult.new exception: Error::Unauthorized.new
    end

    def self.account_transfer( source_account_id, destination_account_id, amount )
      greater_than_minimum? amount
      accounts_exists? source_account_id, destination_account_id
      same_account? source_account_id, destination_account_id
      sufficient_balance? source_account_id, amount unless source_account_id == ROOT_ACCOUNT_ID
      do_transfer! source_account_id, destination_account_id, amount unless destination_account_id == ROOT_ACCOUNT_ID
      ApiResult.new
    rescue StandardError => e
      ApiResult.new exception: e
    end

    def self.account_balance( account_id )
      account = Account.find account_id
      ApiResult.new result: account.balance
    rescue ActiveRecord::RecordNotFound
      e = Error::InexistentAccountID.new account_id
      ApiResult.new exception: e
    end

    private

    def self.same_account?( source_account_id, destination_account_id )
      # Raises if false
      raise Error::SameAccountTransfer if source_account_id == destination_account_id
    end

    def self.accounts_exists?( *account_ids )
      # Receives an integer list
      # Raises if false
      account_ids.each do |account_id|
        unless Account.exists? account_id
          raise Error::InexistentAccountID, account_id
        end
      end
    end

    def self.greater_than_minimum?( amount )
      # Raises if false
      unless amount > MINIMUM_AMOUNT
        raise Error::LessThanMinimumAmount
      end
    end

    def self.sufficient_balance?( account_id, amount )
      # Raises if false
      acc = Account.find(account_id)
      if acc.balance < amount
        raise Error::InsufficientBalance, account_id
      end
    end

    def self.do_transfer!( source_account_id, destination_account_id, amount )
      # Can raise if DB commit fails
      t = Transaction.new(
        source_account_id: source_account_id,
        destination_account_id: destination_account_id,
        amount: amount
      )
      t.save!
    end
  end

end
