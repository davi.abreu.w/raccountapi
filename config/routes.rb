Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :accounts, only: [:show] do
        post 'transfer/', to: 'transactions#create'
      end
      post 'authenticate/', to: 'authentications#authenticate'
    end
  end
end
