class SeedApplicationSample < ActiveRecord::Migration[5.2]
  def change
    Rails.application.load_seed
  end
end
