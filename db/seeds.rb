# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).

# Root Account | to all environments
Account.create(id: 0)
Account.create(id: 1)
Account.create(id: 2)
Account.create(id: 3)

User.create!(login: 'master' , password: 'testapi', password_confirmation: 'testapi')