# README

This guide assumes you're using Linux. Any adaptations needed for other systems is on you.

I recommend you to use Rubymine IDE to help you with almost all tasks in development, tests and deploy.
I was testing a lot of tools, but any other always have something it lacks.

This application is a discover API to learn Rails.
It expose some routes HTTP to use the features of Rails.
It will be covered in next sessions.

All the project comes with a docker-compose setup to make our life easier.

For now you can consume this application on the fly using the calls presented in this docs.
You only need to change the routes to something like this:

curl -H "Content-Type: application/json" -X POST -d '{"password":"testapi"}' https://raccount-api.herokuapp.com/api/v1/authenticate

It's hosted in https://raccount-api.herokuapp.com/ name.

# Version
    The development versions are:
        Ruby: 2.6.2
        Rails: 5.2.3
    To the docker ruby version is:
        Ruby: 2.6.3
        Mainly because it's the official tag supported to 2.6.

# Dependencies

Mainly you need:<br/>
    docker<br/>
    and<br/>
    docker-compose<br/>

If you want to run locally on your environment, just go with the Ruby and Rails versions presented above.

# Configuration

To run using docker, you can just run:<br/>
    docker-compose up

It will already build the docker image from ruby and run our application.<br/>
After this you just need to consume the API through localhost:3000<br/>
If you change the code you need to rebuild the docker image:<br/>
    docker-compose build app

To setup locally, you will need to set up the postgres DB, I will not cover it in here, but for your sake I let a docker service:<br/>
    docker-compose up database

Then go with:<br/>
    bundle install
    rake db:setup
    rails server

# Running tests
We are using RSpec here.<br/>
The easiest way is to run:<br/>
    rails spec

You can check how to run individual tests searching for this directives.<br/>

# The API:
    It's an account manager.
    We do not express in any other place, but all operations are assumed to be in R$ cents.
    So it's a responsibility of the client how to present this data to users.

    You can do 3 things with the API.

    1. Authenticate:
        Otherwise you can do nothing else.
    2. Check the balance of your account:
        All them comes with a balance of 0.
    3. Do transferences to other accounts:
        Assuming you have enough balance.

    To start, all the accounts are with 0. So you can think you can't do nothing.
    To solve this, account 0 is assumed as root.
    Root account can:
        Transfer any amount to anyone;
    and can not:
        Receive any transference;

    The API comes with a presetted set of data, and you can't add anything else than transferences:
    Account 0 : root
    Account 1
    Account 2
    Account 3

    User
        login: master
        password: testapi


# Consuming the API:
    It's all over HTTP following RESTFull principles.
    The responses always are in JSON following the pattern:

    {
        "response":                     # Default
        {  
            "result":null,              # If applicable (the type of request dictates type returned). Default: null
            "status":                   # Always returned
            {  
                "code":0,               # Integer. The id of success or any error found in the operation.
                "message":"Success"     # String. Human readable status of success or error.
            }
        }
    }

    See that there is no error detailment usefull from Machine to Machine to handle errors beyond the error code. This is something to improve.

## Return codes:

    Success:
        API code: 0
        HTTP status: 200

    Transference from same account to same account:
        API code: 50
        HTTP status: 400

    Transference with less than minimum accepted amount:
        API code: 100
        HTTP status: 400

    Insufficient balance to transference:
        API code: 150
        HTTP status: 400
    
    Unauthorized operation:
        API code: 200
        HTTP status: 401

    Inexistent account id:
        API code: 250
        HTTP status: 404
    
    Parameter error:
        API code: 300
        HTTP status: 422
    
    Parameter missing:
        API code: 350
        HTTP status: 422

    Unexpected server error:
        API code: -1
        HTTP status: 500
    
## Operations and routes:
    When a data is requested in body, you just need to set the Content-Type in header.
    The application will know how to handle the data you sent.


### Authenticate:
    curl -H "Content-Type: application/json" -X POST -d '{"login":"master","password":"testapi"}' <domain>/api/v1/authenticate
    Expected result:
        {
            "response":
            {  
                "result": <String: your token>,
                "status":
                {  
                    "code":0,
                    "message":"Success"
                }
            }
        }

### Counsult balance:
    curl -H "Authorization: <your token>" <domain>/api/v1/accounts/<Integer: account id>
    Expected result:
        {
            "response":
            {  
                "result": <Integer: account balance>,
                "status":
                {  
                    "code":0,
                    "message":"Success"
                }
            }
        }

### Make transference:
    curl -H "Authorization: <your token>" -d "to_account_id=<Integer>&amount=<Integer>" <domain>/api/v1/accounts/<account_id>/transfer
    Expected result:
        {
            "response":
            {  
                "result": null,
                "status":
                {  
                    "code":0,
                    "message":"Success"
                }
            }
        }

### In case of errors:
    I'll not cover the possible errors the API can give you.
    But anytime an error is given, the API will not completed the request operation.
